# undine-client

> Client software with GUI for Sleep Monitoring with a RaspberryPI

## Key Features

- MySQL-based readers for popular sleep monitoring hardware.
- tkinter-based GUI.

## Installation

The package can be easily installed via pip. 

```sh
$ pip install undine-client
```

## Credits

The code was written by Alexander Knoll - @aknoll - [alexknoll@mailbox.org](alexknoll@mailbox.org).

## License

This software is distributed under the GPL v3. For details, see [LICENSE](LICENSE).

