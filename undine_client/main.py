#!/usr/bin/env python3
# encoding: utf-8

"""A GUI for sleep monitoring on a RaspberryPI."""

from undine_client.gui.app import App


def main_gui() -> None:
    """Construct GUI for sleep monitoring and run."""
    app = App()
    app.mainloop()
