#!/usr/bin/env python3
# encoding: utf-8
"""General UI elements like buttons etc."""

import tkinter as tk
from undine_client.utils.global_settings import COLORS


class MenuButton(tk.Button):
    """A simple menu button with custom styling."""

    def __init__(self, *args, **kwargs):
        """Class initialization."""
        tk.Button.__init__(self, *args, **kwargs)

        self['borderwidth'] = 0
        self['highlightthickness'] = 0
        self['bd'] = 0
        self['bg'] = COLORS.bg
        self['fg'] = COLORS.bg

        if 'image' in kwargs:
            self['image'] = kwargs['image']
