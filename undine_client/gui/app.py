#!/usr/bin/env python3
# encoding: utf-8

"""Main GUI for sleep monitoring on a RaspberryPI."""

import tkinter as tk
from undine_client.utils.global_settings import COLORS
from undine_client.gui.main_menu import MainMenu
from undine_client.gui.initial_setup import (FrameSetupDatabase,
                                             FrameSetupClient)


class App(tk.Tk):
    """Main GUI window."""

    def __init__(
        self,
        *args,
        geometry: str = '480x320',
        fullscreen: bool = False,
        **kwargs,
    ) -> None:
        """Class initialization."""
        tk.Tk.__init__(self, *args, **kwargs)

        # Get width and height.
        self.width = int(geometry.split('x')[0])
        self.height = int(geometry.split('x')[1])

        # Run as fullscreen if requested.
        if fullscreen is True:
            self.attributes("-fullscreen", True)
        else:
            self.geometry(geometry)

        # Set background color.
        self.configure(bg=COLORS.bg)

        # Construct container for all GUI elements.
        container = tk.Frame(self, bg=COLORS.bg)
        container.pack(side='top', fill='both', expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        # Initialize frames array.
        initial_frame = MainMenu
        self.frames = {}

        for frame_class in (MainMenu, FrameSetupDatabase, FrameSetupClient):
            frame_instance = frame_class(container, self)

            self.frames[frame_class] = frame_instance
            frame_instance.grid(row=0, column=0, sticky="nsew")

        # Check database integrity.
        frame_setup_db = self.frames[FrameSetupDatabase]
        frame_setup_client = self.frames[FrameSetupClient]

        if frame_setup_db.check_database() is False:  # type: ignore
            initial_frame = FrameSetupDatabase   # type: ignore

        # Check client setup.
        elif frame_setup_client.check_client_config() is False:  # type: ignore
            initial_frame = FrameSetupClient   # type: ignore

        self.show_frame(initial_frame)

    def show_frame(self, cont):
        """Display a frame as part of `self`."""
        frame = self.frames[cont]
        frame.tkraise()
