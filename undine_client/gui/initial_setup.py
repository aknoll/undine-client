#!/usr/bin/env python3
# encoding: utf-8
"""Class for generating the main menu window view."""

import tkinter as tk
# pylint: disable=unused-import
import tkinter.messagebox
from undine_client.utils.global_settings import COLORS, DB_CREDENTIALS_PATH
from undine_client.gui.main_menu import MainMenu
from undine_client.db.settings_database import SettingsDatabase
from undine_client.db.helpers import (DatabaseSetupError,
                                      read_credentials_from_file)


class FrameSetupDatabase(tk.Frame):
    """Setup modal for generating the configuration database."""

    def __init__(self, parent, controller):
        """Class initialization."""
        tk.Frame.__init__(self, parent)

        self.controller = controller

        self.configure(bg=COLORS.bg)

        # Text field for displaying the current error message.
        self.label_errormsg = tk.Label(
            self,
            text='a',
            font=('Arial', 12, 'normal'),
            fg=COLORS.red,
            bg=COLORS.bg,
            width=40,
            anchor='e'
        )
        self.label_errormsg.grid(row=0, column=0, pady=5, columnspan=2)

        # Retry button.
        btn_retry = tk.Button(self, text='Retry',
                              command=self.check_database)
        btn_retry.grid(row=1, column=1)

        # Initialize database.
        btn_init = tk.Button(self, text='Initialize',
                             command=self.initialize_database)
        btn_init.grid(row=1, column=0)

    def initialize_database(self):
        """Set up clear local storage database."""
        cred = read_credentials_from_file(DB_CREDENTIALS_PATH)
        SettingsDatabase(*cred, initialize=True)
        self.check_database()

    def check_database(self):
        """Check local storage database."""
        # Try opening database and catch error.
        try:
            cred = read_credentials_from_file(DB_CREDENTIALS_PATH)
            SettingsDatabase(*cred)
            self.controller.show_frame(FrameSetupClient)
            return True

        except DatabaseSetupError as err:
            self.label_errormsg.configure(text=err)
            return False


class FrameSetupClient(tk.Frame):
    """Setup modal for client settings."""

    def __init__(self, parent, controller):
        """Class initialization."""
        tk.Frame.__init__(self, parent)

        self.controller = controller

        self.configure(bg=COLORS.bg)

        # Text field for displaying the current error message.
        self.label_errormsg = tk.Label(
            self,
            text='Client-Einstellungen:',
            font=('Arial', 16, 'normal'),
            fg=COLORS.text,
            bg=COLORS.bg,
            width=20,
            anchor='e'
        )
        self.label_errormsg.grid(row=0, column=0, pady=5)

        label_clientname = tk.Label(
            self,
            text='Clientname:',
            font=('Arial', 14, 'bold'),
            fg=COLORS.text,
            bg=COLORS.bg,
            width=20,
            anchor='e'
        )
        label_clientname.grid(row=1, column=0, pady=5, padx=5)

        self.entry_clientname = tk.Entry(self)
        self.entry_clientname.grid(row=1, column=1, pady=5)

        label_serverip = tk.Label(
            self,
            text='Server IP:',
            font=('Arial', 14, 'bold'),
            fg=COLORS.text,
            bg=COLORS.bg,
            width=20,
            anchor='e'
        )
        label_serverip.grid(row=2, column=0, pady=5, padx=5)

        self.entry_serverip = tk.Entry(self)
        self.entry_serverip.grid(row=2, column=1, pady=5)

        # Retry button.
        btn_retry = tk.Button(self, text='Speichern',
                              command=self.save)
        btn_retry.grid(row=3, column=0, columnspan=2)

    def check_settings(self):
        """Check settings from user input."""
        clientname = self.entry_clientname.get()
        serverip = self.entry_serverip.get()

        if len(clientname) == 0:
            msg = 'Bitte einen Clientnamen eingeben'
            tk.messagebox.showerror(title='Setup Error', message=msg)
            return False

        if len(serverip) == 0:
            msg = 'Bitte eine URL eingeben'
            tk.messagebox.showerror(title='Setup Error', message=msg)
            return False

        return True

    def save(self):
        """Save settings to database from user input."""
        # Check user input.
        if self.check_settings() is False:
            return

        # Read user input.
        clientname = self.entry_clientname.get()
        serverip = self.entry_serverip.get()

        # Open database.
        cred = read_credentials_from_file(DB_CREDENTIALS_PATH)
        local_db = SettingsDatabase(*cred)

        # Insert input into database.
        sql = f"""
            INSERT INTO client_settings
            VALUES ('clientname', '{clientname}');
        """
        local_db.submit(sql)

        sql = f"""
            INSERT INTO client_settings
            VALUES ('serverip', '{serverip}');
        """
        local_db.submit(sql)

        self.check_client_config()

    def check_client_config(self):
        """Set up clear local storage database."""
        cred = read_credentials_from_file(DB_CREDENTIALS_PATH)
        local_db = SettingsDatabase(*cred)

        # Check if all required fields are present.
        required_fields = ['clientname', 'serverip']
        for field in required_fields:
            sql = f"""
                SELECT settingval
                FROM client_settings
                WHERE settingkey = '{field}';
            """
            res = local_db.submit(sql)

            if len(res) == 0:
                return False

        self.controller.show_frame(MainMenu)
        return True
