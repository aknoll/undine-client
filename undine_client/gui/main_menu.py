#!/usr/bin/env python3
# encoding: utf-8
"""Class for generating the main menu window view."""

import os
import tkinter as tk
from undine_client.utils.global_settings import (COLORS, TIMESTEP,
                                                 CONN_CHECK_URL,
                                                 DB_CREDENTIALS_PATH)
from undine_client.gui.ui_elements import MenuButton
from undine_client.utils.helpers import check_url, shutdown, get_ip_address
from undine_client.db.settings_database import SettingsDatabase
from undine_client.db.helpers import read_credentials_from_file


class MainMenu(tk.Frame):
    """Main menu window view."""

    def __init__(self, parent, controller):
        """Class initialization."""
        tk.Frame.__init__(self, parent)

        self.controller = controller
        self.configure(bg=COLORS.bg)

        left_frame = MainMenuLeftFrame(self)
        left_frame.grid(row=0, column=0, padx=5, pady=5)

        right_frame = MainMenuRightFrame(self)
        right_frame.grid(row=0, column=1, padx=5, pady=5)


# Many UI elements require more than 7 instance attributes.
# pylint: disable=too-many-instance-attributes
class MainMenuLeftFrame(tk.Frame):
    """Left frame of the main window."""

    # pylint: disable=unused-argument
    def __init__(self, parent):
        """Class initialization."""
        tk.Frame.__init__(self, parent)

        self.configure(bg=COLORS.bg)

        # Get client name from database.
        cred = read_credentials_from_file(DB_CREDENTIALS_PATH)
        local_db = SettingsDatabase(*cred)

        sql = """
            SELECT settingval
            FROM client_settings
            WHERE settingkey = 'clientname';
        """
        clientname = local_db.submit(sql)

        title = tk.Label(
            self,
            text=clientname,
            font=('Arial', 25, 'normal'),
            fg=COLORS.text,
            bg=COLORS.bg,
        )
        title.grid(row=0, column=0, columnspan=2, padx=10, pady=10)

        icon_path = os.path.join(os.path.split(__file__)[0], 'icons')

        # WLAN settings.
        self.img_btn_wlan = tk.PhotoImage(file=f'{icon_path}/wlan.png')
        self.btn_wlan = MenuButton(self, image=self.img_btn_wlan)
        self.btn_wlan.grid(row=1, column=0, padx=5, pady=5)

        # Input devices.
        self.img_btn_input = tk.PhotoImage(file=f'{icon_path}/input.png')
        self.btn_input = MenuButton(self, image=self.img_btn_input)
        self.btn_input.grid(row=1, column=1)

        # More settings.
        self.img_btn_settings = tk.PhotoImage(file=f'{icon_path}/settings.png')
        self.btn_settings = MenuButton(self, image=self.img_btn_settings)
        self.btn_settings.grid(row=2, column=0)

        # On/Off button.
        self.img_btn_onoff = tk.PhotoImage(file=f'{icon_path}/onoff.png')
        self.btn_onoff = MenuButton(self, image=self.img_btn_onoff,
                                    command=shutdown)
        self.btn_onoff.grid(row=2, column=1)


class MainMenuRightFrame(tk.Frame):
    """Right frame of the main menu."""

    def __init__(self, parent):
        """Class initialization."""
        tk.Frame.__init__(self, parent)

        self.configure(bg=COLORS.bg)

        # Check internet connection.
        connlabel = tk.Label(
            self,
            text='Netzwerk: ',
            font=('Arial', 14, 'normal'),
            fg=COLORS.text,
            bg=COLORS.bg,
            anchor='e',
            width=10
        )
        connlabel.grid(row=0, column=0)

        self.conn = tk.Label(
            self,
            text='x',
            font=('Arial', 14, 'normal'),
            fg=COLORS.red,
            bg=COLORS.bg,
            anchor='w',
            width=12
        )
        self.conn.grid(row=0, column=1)
        self.update_conn()

        # Check server connection.
        serverlabel = tk.Label(
            self,
            text='Server: ',
            font=('Arial', 14, 'normal'),
            fg=COLORS.text,
            bg=COLORS.bg,
            anchor='e',
            width=10
        )
        serverlabel.grid(row=1, column=0)

        self.server = tk.Label(
            self,
            text='x',
            font=('Arial', 14, 'normal'),
            fg=COLORS.red,
            bg=COLORS.bg,
            anchor='w',
            width=12
        )
        self.server.grid(row=1, column=1)
        self.update_server()

        # Show IP address.
        iplabel = tk.Label(
            self,
            text='IP: ',
            font=('Arial', 14, 'normal'),
            fg=COLORS.text,
            bg=COLORS.bg,
            anchor='e',
            width=10
        )
        iplabel.grid(row=2, column=0)

        self.ipaddress = tk.Label(
            self,
            text='No IP found.',
            font=('Arial', 14, 'normal'),
            fg=COLORS.text,
            bg=COLORS.bg,
            anchor='w',
            width=14
        )
        self.ipaddress.grid(row=2, column=1)
        self.update_ip()

    def update_ip(self):
        """Periodically recheck the IP address of the device."""
        self.ipaddress.config(text=str(get_ip_address()))
        self.after(TIMESTEP, self.update_ip)

    def update_conn(self):
        """Periodically check internet connection of the device."""
        if check_url(CONN_CHECK_URL) is True:
            self.conn.config(text='✓')
            self.conn.config(fg=COLORS.green)
        else:
            self.conn.config(text='x')
            self.conn.config(fg=COLORS.red)

        self.after(TIMESTEP, self.update_conn)

    def update_server(self):
        """Periodically check connection to the upstream server."""
        # Get server url from database.
        cred = read_credentials_from_file(DB_CREDENTIALS_PATH)
        local_db = SettingsDatabase(*cred)

        sql = """
            SELECT settingval
            FROM client_settings
            WHERE settingkey = 'serverip';
        """
        url = local_db.submit(sql)

        if check_url(url) is True:
            self.server.config(text='✓')
            self.server.config(fg=COLORS.green)
        else:
            self.server.config(text='x')
            self.server.config(fg=COLORS.red)

        self.after(TIMESTEP, self.update_conn)
