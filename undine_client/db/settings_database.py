#!/usr/bin/env python3
# encoding: utf-8

"""Local database for setting storage."""

from undine_client.db.abstract import AbstractDatabase


class SettingsDatabase(AbstractDatabase):
    """Local mysql database for storing client settings."""

    tables = {
        'client_settings': {
            'settingkey': 'varchar(50) primary key',
            'settingval': 'varchar(50)'
        }
    }

    def initialize(self):
        """Initialize a clean client settings database."""
        self.create_table_client_settings()

    def create_table_client_settings(self):
        """Create a table for storing key-value pairs of settings."""
        sql = """
            CREATE TABLE client_settings(settingkey varchar(50) primary key,
                                         settingval varchar(50));
        """
        self.submit(sql)
