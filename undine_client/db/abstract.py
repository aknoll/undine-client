#!/usr/bin/env python3
# encoding: utf-8

"""Abstract base class for all databases."""

from typing import Optional, Dict
import mysql
from mysql.connector import errorcode
from undine_client.db.helpers import DatabaseSetupError


class AbstractDatabase:
    """A generic MySQL database."""

    tables: Dict[str, Dict[str, str]] = {}

    def __init__(self, host, name, user, passwd, initialize=False):
        """Class initialization."""
        self.host = host
        self.name = name
        self.user = user
        self.passwd = passwd

        if initialize is True:
            self.initialize()
        else:
            self.check_integrity()

    def initialize(self):
        """Initialize the database tables. Deferred to child classes."""
        raise NotImplementedError()

    def check_integrity(self):
        """Check if the database is set up correctly."""
        # Check whether the database itself exists.
        sql = f"SHOW DATABASES LIKE '{self.name}'"
        res = self.submit(sql, no_database=True)

        if len(res) == 0:
            raise DatabaseSetupError(f'Database {self.name} does not exist.')

        # Check if all needed tables exist.
        existing_tables = self.submit('SHOW tables;')
        for tablename in self.tables:
            if not any(i[0] == tablename for i in existing_tables):
                raise DatabaseSetupError(f'Table {tablename} does not exist.')

    def submit(
        self,
        sql: str,
        no_database: Optional[bool] = False
    ):
        """Send query `sql` to database and retrieve `res`ults."""
        # Some queries might explicitely ask to be executed without any
        # specific database.
        if no_database is True:
            database = None
        else:
            database = self.name

        try:
            conn = mysql.connector.connect(
                host=self.host,
                user=self.user,
                passwd=self.passwd,
                database=database
            )

            cursor = conn.cursor()
            cursor.execute(sql)
            res = cursor.fetchall()
            conn.commit()
            conn.close()

            return res

        except mysql.connector.errors.DatabaseError as err:

            if err.errno == 2003:
                raise DatabaseSetupError(
                    'Local MySQL server cannot be reached.'
                ) from err

            elif err.errno == 1045:
                raise DatabaseSetupError(
                    f'MySQL user "{self.user}" does not exist/has no access.'
                ) from err

            elif err.errno in [errorcode.ER_NO_SUCH_TABLE,
                               errorcode.ER_BAD_FIELD_ERROR]:
                raise DatabaseSetupError(str(err)) from err

            else:
                raise err

    def create_settings_table(self):
        """Create a table for storing key-value pairs of settings."""
        sql = """
            CREATE TABLE client_settings(settingkey varchar(50) primary key,
                                         settingval varchar(50));
        """
        self.submit(sql)
