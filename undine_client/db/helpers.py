#!/usr/bin/env python3
# encoding: utf-8

"""Helper functions for database management."""

from typing import Optional
import os


class DatabaseSetupError(Exception):
    """Error class for catching all mistakes relating to database setup."""

    def __init__(self, message: Optional[str] = None) -> None:
        """Initialize exception with custom error message."""
        if not message:
            message = "Error while trying to open local database."

        super().__init__(message)


def read_credentials_from_file(path):
    """Read MySQL credentials from a ';' delimited text file at `path`."""
    host = None
    user = None
    name = None
    passwd = None

    if not os.path.exists(path):
        raise DatabaseSetupError('Credentials file not found.')

    with open(path, 'r', encoding='utf8') as infile:
        for line in infile:
            key, val = line.split(';')

            if key == 'HOST':
                host = val.strip('\n')
            elif key == 'USERNAME':
                user = val.strip('\n')
            elif key == 'PASSWORD':
                passwd = val.strip('\n')
            elif key == 'DB_NAME':
                name = val.strip('\n')

    if any(i is None for i in (host, name, user, passwd)):
        raise DatabaseSetupError('Insufficient credentials provided.')

    return (host, name, user, passwd)
