#!/usr/bin/env python3
# encoding: utf-8
"""Define a custom colorscheme for the GUI."""

from pathlib import Path

# Timestep for periodically checking connectivity, IP address, etc.
TIMESTEP = 10000

# URL for connectivity check to the internet.
CONN_CHECK_URL = 'https://www.gnu.org'

# URL of the upstream database server.
SERVER_URL = 'https://www.undine-monitor.de'

# Database settings.
DB_HOST = 'localhost'
DB_NAME = 'undineclient'
DB_CREDENTIALS_PATH = f'{Path.home()}/.undine-credentials.txt'


# Pure storage class does not need any public methods.
# pylint: disable=too-few-public-methods
class ColorScheme:
    """Global color settings."""

    bg = '#2A2C25'
    red = '#af1e2b'
    blue = '#437bc6'
    green = '#227b2b'
    yellow = '#af7b2b'
    text = '#ffffff'


COLORS = ColorScheme()
