#!/usr/bin/env python3
# encoding: utf-8
"""Simple helper functions."""

import os
import socket
import tkinter as tk
# pylint: disable=unused-import
import tkinter.messagebox
import requests


def shutdown() -> None:
    """Send shutdown request to system."""
    box = tk.messagebox.askyesno(
        title='Herunterfahren?',
        message='Soll der PC heruntergefahren werden?'
    )

    if box is True:
        os.system('sudo shutdown now')


def get_ip_address() -> str:
    """Resolve own ip address."""
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        try:
            sock.connect(('www.gnu.org', 80))
            ip_address = str(sock.getsockname()[0])

        # In order to prevent the program from terminating because the internet
        # cannot be reached we simply ignore all exceptions.
        # pylint: disable=broad-exception-caught
        except Exception:
            ip_address = 'not found'

    return ip_address


def check_url(url: str) -> bool:
    """Check whether `url` can be reached or not."""
    try:
        code = requests.head(url, timeout=1).status_code

        if code == 200:
            return True

    # In order to prevent the program from terminating because the internet
    # cannot be reached we simply ignore all exceptions.
    # pylint: disable=broad-exception-caught
    except Exception:
        return False

    return False
