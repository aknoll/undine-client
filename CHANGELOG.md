# Changelog
All notable changes to this project will be documented in this file.

The format of this file is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2023-04-06
### Added
- The first standalone version of this project.

[Unreleased]: https://gitlab.com/runner-suite/runnerase/-/compare/0.1.0...main
